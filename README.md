# Bluetooth Heart Rate App
A small project to query the heartbeat from a BluetoothLE device. 

## The following guides were leading for the implementation

[Shahar Avigezer](https://medium.com/@shahar_avigezer/bluetooth-low-energy-on-android-22bc7310387a) \
[Bluetooth Low Energy](https://developer.android.com/guide/topics/connectivity/bluetooth/ble-overview) \
[Stuart Kent](https://youtu.be/jDykHjn-4Ng) \
[Programcreek](https://www.programcreek.com/java-api-examples/?class=android.bluetooth.BluetoothGattCharacteristic&method=FORMAT_UINT8)
